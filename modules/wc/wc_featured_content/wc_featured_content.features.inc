<?php
/**
 * @file
 * wc_featured_content.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wc_featured_content_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_fe_nodequeue_export_fields().
 */
function wc_featured_content_fe_nodequeue_export_fields() {
  $nodequeues = array();

  // Exported nodequeues: wc_featured_content
  $nodequeues['wc_featured_content'] = array(
    'name' => 'wc_featured_content',
    'title' => 'Featured content',
    'subqueue_title' => '',
    'size' => 0,
    'link' => '',
    'link_remove' => '',
    'owner' => 'nodequeue',
    'show_in_ui' => 1,
    'show_in_tab' => 1,
    'show_in_links' => 0,
    'reference' => 0,
    'reverse' => 0,
    'i18n' => 0,
    'subqueues' => 1,
    'types' => array(
      0 => 'article',
      1 => 'event',
      2 => 'forum',
      3 => 'media_gallery',
      4 => 'podcast',
      5 => 'series',
      6 => 'video',
    ),
    'roles' => array(),
    'count' => 0,
  );

  return $nodequeues;
}

/**
 * Implements hook_flag_default_flags().
 */
function wc_featured_content_flag_default_flags() {
  $flags = array();
  // Exported flag: "Featured post".
  $flags['feature'] = array(
    'content_type' => 'node',
    'title' => 'Featured post',
    'global' => 1,
    'types' => array(
      0 => 'article',
      1 => 'event',
      2 => 'forum',
      3 => 'media_gallery',
      4 => 'podcast',
      5 => 'series',
      6 => 'video',
    ),
    'flag_short' => 'Feature on the front page',
    'flag_long' => 'Marking the post as a feature places it into the queue for featured posts on the front page.',
    'flag_message' => '',
    'unflag_short' => 'Remove from the front page',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(),
      'unflag' => array(),
    ),
    'weight' => 0,
    'show_on_form' => 1,
    'access_author' => '',
    'show_on_page' => 0,
    'show_on_teaser' => 0,
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'wc_featured_content',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;

}
