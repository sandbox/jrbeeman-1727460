<?php
/**
 * @file
 * wc_event_content_type.metatag.inc
 */

/**
 * Implements hook_metatag_config_default().
 */
function wc_event_content_type_metatag_config_default() {
  $export = array();

  $config = new stdClass();
  $config->disabled = FALSE; /* Edit this to true to make a default config disabled initially */
  $config->api_version = 1;
  $config->cid = '2';
  $config->instance = 'node:event';
  $config->config = array(
    'keywords' => array(
      'value' => '[node:field_tags]',
    ),
    'og:type' => array(
      'value' => 'activity',
    ),
    'og:image' => array(
      'value' => '[node:field_hero]',
    ),
  );
  $export['node:event'] = $config;

  return $export;
}
