<?php
/**
 * @file
 * wc_userpoints_ranks.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function wc_userpoints_ranks_default_rules_configuration() {
  $items = array();
  $items['rules_wc_userpoints_rank_comment'] = entity_import('rules_config', '{ "rules_wc_userpoints_rank_comment" : {
      "LABEL" : "wc_userpoints_rank_comment",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Watercooler" ],
      "REQUIRES" : [ "userpoints_rules", "comment" ],
      "ON" : { "comment_insert" : [] },
      "DO" : [
        { "userpoints_action_grant_points" : {
            "user" : [ "comment:author" ],
            "points" : "1",
            "tid" : "424",
            "entity" : [ "comment" ],
            "operation" : "Add 1 point",
            "display" : 0,
            "moderate" : "approved"
          }
        }
      ]
    }
  }');
  $items['rules_wc_userpoints_rank_grant_1'] = entity_import('rules_config', '{ "rules_wc_userpoints_rank_grant_1" : {
      "LABEL" : "wc_userpoints_rank_grant_1",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Watercooler" ],
      "REQUIRES" : [ "rules", "userpoints_rules" ],
      "ON" : { "userpoints_event_points_awarded_after" : [] },
      "IF" : [
        { "NOT data_is" : { "data" : [ "userpoints-transaction:user:field-rank" ], "value" : "88" } },
        { "userpoints_condition_check_points" : [] },
        { "userpoints_condition_check_points" : [] }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "userpoints-transaction:user:field-rank" ], "value" : "88" } }
      ]
    }
  }');
  $items['rules_wc_userpoints_rank_grant_2'] = entity_import('rules_config', '{ "rules_wc_userpoints_rank_grant_2" : {
      "LABEL" : "wc_userpoints_rank_grant_2",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Watercooler" ],
      "REQUIRES" : [ "rules", "userpoints_rules" ],
      "ON" : { "userpoints_event_points_awarded_after" : [] },
      "IF" : [
        { "NOT data_is" : { "data" : [ "userpoints-transaction:user:field-rank" ], "value" : "89" } },
        { "userpoints_condition_check_points" : [] },
        { "userpoints_condition_check_points" : [] }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "userpoints-transaction:user:field-rank" ], "value" : "89" } }
      ]
    }
  }');
  $items['rules_wc_userpoints_rank_grant_3'] = entity_import('rules_config', '{ "rules_wc_userpoints_rank_grant_3" : {
      "LABEL" : "wc_userpoints_rank_grant_3",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Watercooler" ],
      "REQUIRES" : [ "rules", "userpoints_rules" ],
      "ON" : { "userpoints_event_points_awarded_after" : [] },
      "IF" : [
        { "NOT data_is" : { "data" : [ "userpoints-transaction:user:field-rank" ], "value" : "90" } },
        { "userpoints_condition_check_points" : [] },
        { "userpoints_condition_check_points" : [] }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "userpoints-transaction:user:field-rank" ], "value" : "90" } }
      ]
    }
  }');
  $items['rules_wc_userpoints_rank_grant_4'] = entity_import('rules_config', '{ "rules_wc_userpoints_rank_grant_4" : {
      "LABEL" : "wc_userpoints_rank_grant_4",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Watercooler" ],
      "REQUIRES" : [ "rules", "userpoints_rules" ],
      "ON" : { "userpoints_event_points_awarded_after" : [] },
      "IF" : [
        { "NOT data_is" : { "data" : [ "userpoints-transaction:user:field-rank" ], "value" : "91" } },
        { "userpoints_condition_check_points" : [] },
        { "userpoints_condition_check_points" : [] }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "userpoints-transaction:user:field-rank" ], "value" : "91" } }
      ]
    }
  }');
  $items['rules_wc_userpoints_rank_grant_5'] = entity_import('rules_config', '{ "rules_wc_userpoints_rank_grant_5" : {
      "LABEL" : "wc_userpoints_rank_grant_5",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Watercooler" ],
      "REQUIRES" : [ "rules", "userpoints_rules" ],
      "ON" : { "userpoints_event_points_awarded_after" : [] },
      "IF" : [
        { "NOT data_is" : { "data" : [ "userpoints-transaction:user:field-rank" ], "value" : "92" } },
        { "userpoints_condition_check_points" : [] },
        { "userpoints_condition_check_points" : [] }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "userpoints-transaction:user:field-rank" ], "value" : "92" } }
      ]
    }
  }');
  $items['rules_wc_userpoints_rank_grant_6'] = entity_import('rules_config', '{ "rules_wc_userpoints_rank_grant_6" : {
      "LABEL" : "wc_userpoints_rank_grant_6",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Watercooler" ],
      "REQUIRES" : [ "rules", "userpoints_rules" ],
      "ON" : { "userpoints_event_points_awarded_after" : [] },
      "IF" : [
        { "NOT data_is" : { "data" : [ "userpoints-transaction:user:field-rank" ], "value" : "93" } },
        { "userpoints_condition_check_points" : [] }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "userpoints-transaction:user:field-rank" ], "value" : "93" } }
      ]
    }
  }');
  $items['rules_wc_userpoints_rank_node'] = entity_import('rules_config', '{ "rules_wc_userpoints_rank_node" : {
      "LABEL" : "wc_userpoints_rank_node",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Watercooler" ],
      "REQUIRES" : [ "userpoints_rules", "rules" ],
      "ON" : { "node_insert" : [] },
      "DO" : [
        { "userpoints_action_grant_points" : {
            "user" : [ "node:author" ],
            "points" : "1",
            "tid" : "424",
            "entity" : [ "node" ],
            "operation" : "Add 1 point",
            "display" : 0,
            "moderate" : "approved"
          }
        }
      ]
    }
  }');
  return $items;
}
