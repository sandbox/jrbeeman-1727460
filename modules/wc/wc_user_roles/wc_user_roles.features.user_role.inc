<?php
/**
 * @file
 * wc_user_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function wc_user_roles_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => '12',
  );

  // Exported role: developer.
  $roles['developer'] = array(
    'name' => 'developer',
    'weight' => '13',
  );

  // Exported role: editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => '14',
  );

  // Exported role: reviewer.
  $roles['reviewer'] = array(
    'name' => 'reviewer',
    'weight' => '9',
  );

  // Exported role: troll.
  $roles['troll'] = array(
    'name' => 'troll',
    'weight' => '15',
  );

  return $roles;
}
