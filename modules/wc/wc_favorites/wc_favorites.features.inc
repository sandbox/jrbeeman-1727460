<?php
/**
 * @file
 * wc_favorites.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wc_favorites_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function wc_favorites_flag_default_flags() {
  $flags = array();
  // Exported flag: "Favorite".
  $flags['favorite'] = array(
    'content_type' => 'node',
    'title' => 'Favorite',
    'global' => 0,
    'types' => array(
      0 => 'article',
      1 => 'event',
      2 => 'forum',
      3 => 'media_gallery',
      4 => 'podcast',
      5 => 'series',
      6 => 'video',
    ),
    'flag_short' => 'Add to favorites',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Remove from favorites',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => 2,
      ),
      'unflag' => array(
        0 => 2,
      ),
    ),
    'weight' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'wc_favorites',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;

}
