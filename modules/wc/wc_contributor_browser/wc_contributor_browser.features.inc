<?php
/**
 * @file
 * wc_contributor_browser.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wc_contributor_browser_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
