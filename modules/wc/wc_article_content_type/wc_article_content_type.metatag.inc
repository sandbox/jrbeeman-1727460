<?php
/**
 * @file
 * wc_article_content_type.metatag.inc
 */

/**
 * Implements hook_metatag_config_default().
 */
function wc_article_content_type_metatag_config_default() {
  $export = array();

  $config = new stdClass();
  $config->disabled = FALSE; /* Edit this to true to make a default config disabled initially */
  $config->api_version = 1;
  $config->cid = '1';
  $config->instance = 'node:article';
  $config->config = array(
    'keywords' => array(
      'value' => '[node:field_tags]',
    ),
    'og:image' => array(
      'value' => '[node:field_hero]',
    ),
  );
  $export['node:article'] = $config;

  return $export;
}
