(function ($) {

  /**
   * See http://phpjs.org/functions/urldecode/
   */
  function wc_quote_urldecode(str) {
    return decodeURIComponent((str + '').replace(/%(?![\da-f]{2})/gi, function () {
      // PHP tolerates poorly formed escape sequences
      return '%25';
    }).replace(/\+/g, '%20'));
  }

  Drupal.behaviors.wcQuote = {
    attach: function () {
      var textarea_selector = '.comment-form .field-name-comment-body textarea.markItUpEditor';
      // Retrieve and set the quote contents.
      $('a.wc-quote').click( function() {
        var text = $(this).attr('data-quote');
        if (text) {
          text = wc_quote_urldecode(text);
          var current_val = $(textarea_selector).val().trim();
          var new_val = current_val;
          // Add newlines if the text area already has a value.
          if (new_val) {
            new_val = new_val + "\n\n" + text;
          }
          else {
            new_val = text;
          }
          // Set the textarea with the new value.
          $(textarea_selector).val(new_val);
        }
        return false;
      });
    }
  }

})(jQuery);