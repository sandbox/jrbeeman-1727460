<?php
/**
 * @file
 * wc_media_gallery_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_media_gallery_content_type_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wc_media_gallery_content_type_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wc_media_gallery_content_type_node_info() {
  $items = array(
    'media_gallery' => array(
      'name' => t('Media gallery'),
      'base' => 'node_content',
      'description' => t('A media gallery is a collection of images or video.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
