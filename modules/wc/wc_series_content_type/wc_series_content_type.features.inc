<?php
/**
 * @file
 * wc_series_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wc_series_content_type_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function wc_series_content_type_node_info() {
  $items = array(
    'series' => array(
      'name' => t('Series'),
      'base' => 'node_content',
      'description' => t('A series is a manually-curated collection of content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
