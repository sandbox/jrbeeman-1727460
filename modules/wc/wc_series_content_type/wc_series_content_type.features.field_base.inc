<?php
/**
 * @file
 * wc_series_content_type.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function wc_series_content_type_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_series_items'
  $field_bases['field_series_items'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_series_items',
    'foreign keys' => array(),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'direction' => 'ASC',
          'property' => 'title',
          'type' => 'property',
        ),
        'target_bundles' => array(
          'article' => 'article',
          'event' => 'event',
          'forum' => 'forum',
          'podcast' => 'podcast',
          'video' => 'video',
        ),
      ),
      'profile2_private' => FALSE,
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
