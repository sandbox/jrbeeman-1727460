<?php
/**
 * @file
 * wc_reported_content.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function wc_reported_content_default_rules_configuration() {
  $items = array();
  $items['rules_wc_reported_comment'] = entity_import('rules_config', '{ "rules_wc_reported_comment" : {
      "LABEL" : "wc_reported_comment",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Watercooler" ],
      "REQUIRES" : [ "flag", "rules" ],
      "ON" : { "flag_flagged_report_comment" : [] },
      "IF" : [
        { "flag_threshold_comment" : {
            "flag" : "report_comment",
            "comment" : [ "flagged-comment" ],
            "number" : "10",
            "operator" : "\\u003E="
          }
        }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "flagged-comment:status" ], "value" : "0" } }
      ]
    }
  }');
  $items['rules_wc_reported_node'] = entity_import('rules_config', '{ "rules_wc_reported_node" : {
      "LABEL" : "wc_reported_node",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Watercooler" ],
      "REQUIRES" : [ "flag", "rules" ],
      "ON" : { "flag_flagged_report_content" : [] },
      "IF" : [
        { "flag_threshold_node" : {
            "flag" : "report_content",
            "node" : [ "flagged-node" ],
            "number" : "10",
            "operator" : "\\u003E="
          }
        }
      ],
      "DO" : [ { "node_unpublish" : { "node" : [ "flagged-node" ] } } ]
    }
  }');
  $items['rules_wc_reported_user'] = entity_import('rules_config', '{ "rules_wc_reported_user" : {
      "LABEL" : "wc_reported_user",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "Watercooler" ],
      "REQUIRES" : [ "flag", "rules" ],
      "ON" : { "flag_flagged_report_user" : [] },
      "IF" : [
        { "flag_threshold_user" : {
            "flag" : "report_user",
            "user" : [ "flagged-user" ],
            "number" : "10",
            "operator" : "\\u003E="
          }
        }
      ],
      "DO" : [ { "user_block" : { "account" : [ "flagged-user" ] } } ]
    }
  }');
  return $items;
}
