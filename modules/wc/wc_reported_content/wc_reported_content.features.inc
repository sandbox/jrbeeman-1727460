<?php
/**
 * @file
 * wc_reported_content.features.inc
 */

/**
 * Implements hook_views_api().
 */
function wc_reported_content_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function wc_reported_content_flag_default_flags() {
  $flags = array();
  // Exported flag: "Report comment".
  $flags['report_comment'] = array(
    'content_type' => 'comment',
    'title' => 'Report comment',
    'global' => 0,
    'types' => array(
      0 => 'comment_node_article',
      1 => 'comment_node_event',
      2 => 'comment_node_forum',
      3 => 'comment_node_media_gallery',
      4 => 'comment_node_podcast',
      5 => 'comment_node_poll',
      6 => 'comment_node_series',
      7 => 'comment_node_video',
    ),
    'flag_short' => 'Report',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Un-report',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => 2,
      ),
      'unflag' => array(
        0 => 2,
      ),
    ),
    'weight' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_comment' => 1,
    'module' => 'wc_reported_content',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Report content".
  $flags['report_content'] = array(
    'content_type' => 'node',
    'title' => 'Report content',
    'global' => 0,
    'types' => array(
      0 => 'event',
      1 => 'forum',
    ),
    'flag_short' => 'Report',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Un-report',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => 2,
      ),
      'unflag' => array(
        0 => 2,
      ),
    ),
    'weight' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_page' => 1,
    'show_on_teaser' => 0,
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'wc_reported_content',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  // Exported flag: "Report user".
  $flags['report_user'] = array(
    'content_type' => 'user',
    'title' => 'Report user',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Report',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Un-report',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => 2,
      ),
      'unflag' => array(
        0 => 2,
      ),
    ),
    'weight' => 0,
    'show_on_entity' => FALSE,
    'show_on_form' => 0,
    'access_author' => '',
    'show_on_profile' => 1,
    'access_uid' => 'others',
    'module' => 'wc_reported_content',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;

}
