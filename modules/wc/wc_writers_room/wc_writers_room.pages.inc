<?php

/**
 * @file
 * Writers room page callbacks.
 */

function wc_writers_room_admin_settings() {
  $vid = variable_get('forum_nav_vocabulary', 0);
  $terms = taxonomy_get_tree($vid);
  $options = array();
  foreach ($terms as $term) {
    $options[$term->tid] = $term->name;
  }

  $form = array();

  $form['wc_writers_room_term'] = array(
    '#type' => 'select',
    '#title' => t('Writers rooms forum terms'),
    '#description' => t('Select one or more forums that will act as writers rooms.'),
    '#attributes' => array(),
    '#options' => $options,
    '#default_value' => variable_get('wc_writers_room_term', array()),
    '#multiple' => TRUE,
  );

  $form['wc_admins_room_term'] = array(
    '#type' => 'select',
    '#title' => t('Administrator rooms forum terms'),
    '#description' => t('Select one or more forums that will act as administrator rooms.'),
    '#attributes' => array(),
    '#options' => $options,
    '#default_value' => variable_get('wc_admins_room_term', array()),
    '#multiple' => TRUE,
  );

  return system_settings_form($form);
}
