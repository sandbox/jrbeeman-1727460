# About

See build-watercooler.make for the list of included projects.

For details on how this works, see http://drupal.org/node/1476014
and the great install profile sample project, Commons:
http://drupalcode.org/project/commons.git/tree/refs/heads/7.x-3.x


# Building the project

If you want to just quickly build the project

1. Download the build-watercooler.make file
2. Run `drush make build-watercooler.make watercooler`
3. The folder `watercooler` will now contain the full Drupal distribution

To create a working copy, run:

    drush make build-watercooler.make ./watercooler --working-copy --no-gitinfofile


# Reporting issues

Report issues on Drupal.org: https://drupal.org/sandbox/jrbeeman/1727460
