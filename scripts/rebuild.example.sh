#!/bin/sh

# Change permissions on sites folder to avoid permissions when deleting the folder.
chmod -R gou+rwx ~/Sites/watercooler/sites/;

# Remove the existing install.
rm -Rf ~/Sites/watercooler;

# Rebuild.
drush make ~/Projects/drupal/watercooler/build-watercooler.make ~/Sites/watercooler --working-copy --no-gitinfofile;

# Set upstream remotes to which we can push various in-development projects.
# Watercooler.
cd ~/Sites/watercooler/profiles/watercooler/;
git remote add upstream jrbeeman@git.drupal.org:sandbox/jrbeeman/1727460.git;
# CKEditor BBCode.
cd ~/Sites/watercooler/profiles/watercooler/modules/custom/ckeditor_bbcode/;
git remote add upstream jrbeeman@git.drupal.org:sandbox/jrbeeman/2218053.git;
# Fast forum.
cd ~/Sites/watercooler/profiles/watercooler/modules/custom/fast_forum/;
git remote add upstream jrbeeman@git.drupal.org:sandbox/jrbeeman/2073961.git;

# Reinstall the site.
drush @loc.watercooler si -yv watercooler --site-name="Watercooler (local)" --db-url="mysql://drupaluser:@127.0.0.1:33067/watercooler";

# Enable developer tools.
drush @loc.watercooler en faker forum_generate devel xhprof -y;