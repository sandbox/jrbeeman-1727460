api = 2
core = 7.x

includes[] = "drupal-org-core.make"


projects[watercooler][type] = "profile"
projects[watercooler][download][type] = "git"
projects[watercooler][download][url] = "http://git.drupal.org/sandbox/jrbeeman/1727460.git"
projects[watercooler][download][branch] = "7.x-1.x"
