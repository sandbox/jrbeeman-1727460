api = 2
core = 7.x

; General contrib modules
projects[acquia_connector][subdir] = contrib
projects[acquia_connector][version] = 2.14

projects[admin_icons][type] = "module"
projects[admin_icons][subdir] = "contrib"
projects[admin_icons][download][type] = "git"
projects[admin_icons][download][url] = "http://git.drupal.org/project/admin_icons.git"
projects[admin_icons][download][branch] = "7.x-1.x"
projects[admin_icons][download][revision] = "60d9f28801533fecc92216a60d444d89d80e7611"

projects[apachesolr][subdir] = contrib
projects[apachesolr][version] = 1.6

projects[apachesolr_user][type] = "module"
projects[apachesolr_user][subdir] = "contrib"
projects[apachesolr_user][download][type] = "git"
projects[apachesolr_user][download][url] = "http://git.drupal.org/project/apachesolr_user.git"
projects[apachesolr_user][download][branch] = "7.x-1.x"
projects[apachesolr_user][download]revision] = "a86c5aebfceaf4a3fc53544762a36ca1b70809d5"

; Check the user object before trying to display a result.
; https://drupal.org/node/2077281#comment-7807937
projects[apachesolr_user][patch][] = "http://drupal.org/files/2077281-apache-solr-user-check-3.patch"

projects[autocomplete_deluxe][subdir] = contrib
projects[autocomplete_deluxe][version] = 2.0-beta3

projects[breakpoints][type] = "module"
projects[breakpoints][subdir] = "contrib"
projects[breakpoints][download][type] = "git"
projects[breakpoints][download][url] = "http://git.drupal.org/project/breakpoints.git"
projects[breakpoints][download][branch] = "7.x-1.x"
projects[breakpoints][download][revision] = "c4f3665"

projects[ckeditor][type] = "module"
projects[ckeditor][subdir] = "contrib"
projects[ckeditor][download][type] = "git"
projects[ckeditor][download][url] = "http://git.drupal.org/project/ckeditor.git"
projects[ckeditor][download][branch] = "7.x-1.x"
projects[ckeditor][download][revision] = "b69a9ac"

; Only load CSS when necessary.
; https://drupal.org/node/1370894
projects[ckeditor][patch][] = "https://drupal.org/files/issues/attach-ckeditor-css-1370894-8.patch"

; Accomodate latest Media changes.
; https://drupal.org/node/2159403
projects[ckeditor][patch][] = "https://drupal.org/files/issues/ckeditor-accomodate-latest-media-changes-0.patch"

; Remove redundant external plugin declarations.
; https://drupal.org/comment/8284591#comment-8284591
projects[ckeditor][patch][] = "https://drupal.org/files/issues/ckeditor-remove-external-plugin-declarations-1-alt.patch"

projects[ckeditor_bbcode][subdir] = custom
projects[ckeditor_bbcode][type] = module
projects[ckeditor_bbcode][download][type] = git
projects[ckeditor_bbcode][download][url] = http://git.drupal.org/sandbox/jrbeeman/2218053.git
projects[ckeditor_bbcode][download][branch] = 7.x-1.x

projects[comment_notify][subdir] = contrib
projects[comment_notify][version] = 1.2

projects[ctools][subdir] = contrib
projects[ctools][version] = 1.4

projects[date][subdir] = contrib
projects[date][version] = 2.7

projects[date_facets][type] = "module"
projects[date_facets][subdir] = "contrib"
projects[date_facets][version] = "1.0-beta2"

projects[diff][subdir] = contrib
projects[diff][version] = 3.2

projects[edit_profile][subdir] = contrib
projects[edit_profile][version] = 1.0-beta2

projects[entity][subdir] = contrib
projects[entity][version] = 1.5

projects[entitycache][subdir] = contrib
projects[entitycache][version] = 1.2

projects[entityreference][subdir] = contrib
projects[entityreference][version] = 1.1

projects[escape_admin][version] = "1.x-dev"
projects[escape_admin][type] = "module"
projects[escape_admin][subdir] = "contrib"
projects[escape_admin][download][type] = "git"
projects[escape_admin][download][revision] = "ecd3f58"
projects[escape_admin][download][branch] = "7.x-1.x"

projects[facetapi][subdir] = contrib
projects[facetapi][version] = 1.3

projects[fast_forum][subdir] = custom
projects[fast_forum][type] = module
projects[fast_forum][download][type] = git
projects[fast_forum][download][url] = http://git.drupal.org/sandbox/jrbeeman/2073961.git
projects[fast_forum][download][branch] = 7.x-1.x

projects[features][subdir] = contrib
projects[features][version] = 2.0

projects[features_extra][subdir] = contrib
projects[features_extra][version] = 1.0-beta1

projects[features_override][subdir] = contrib
projects[features_override][version] = 2.0-rc1

projects[feeds][subdir] = contrib
projects[feeds][version] = 2.0-alpha8

projects[field_collection][subdir] = contrib
projects[field_collection][version] = 1.0-beta7

projects[field_group][subdir] = contrib
projects[field_group][version] = 1.4

projects[file_entity][subdir] = contrib
projects[file_entity][version] = 2.0-alpha3

projects[flag][subdir] = contrib
projects[flag][version] = 2.2

projects[focal_point][subdir] = contrib
projects[focal_point][version] = 1.0-alpha4

projects[google_analytics][subdir] = contrib
projects[google_analytics][version] = 1.3

projects[inline_entity_form][subdir] = contrib
projects[inline_entity_form][version] = 1.5

projects[jquery_update][version] = "2.3"
projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = "contrib"

projects[libraries][subdir] = contrib
projects[libraries][version] = 2.2

projects[link][subdir] = contrib
projects[link][version] = 1.2

projects[markdown][subdir] = contrib
projects[markdown][version] = 1.2

projects[media][type] = "module"
projects[media][subdir] = "contrib"
projects[media][download][type] = "git"
projects[media][download][url] = "http://git.drupal.org/project/media.git"
projects[media][download][branch] = "7.x-2.x"
projects[media][download][revision] = "b2c2d78"

; New filelfield browser widget is massively confusing the user
; https://drupal.org/comment/8570379#comment-8570379
projects[media][patch][] = "https://drupal.org/files/issues/automatically-attach-files-2216329-2.patch"

; Improve UX for Media Thumbnail and Media Bulk Upload's multiform page
; http://drupal.org/node/2166623
projects[media][patch][2166623] = "http://drupal.org/files/issues/media_bulk_upload-improve-multiform-2166623-2.patch"

projects[media_oembed][type] = "module"
projects[media_oembed][subdir] = "contrib"
projects[media_oembed][version] = "2.1"

projects[media_youtube][version] = "2.x-dev"
projects[media_youtube][type] = "module"
projects[media_youtube][subdir] = "contrib"
projects[media_youtube][download][type] = "git"
projects[media_youtube][download][revision] = "187283f"
projects[media_youtube][download][branch] = "7.x-2.x"

projects[memcache][subdir] = contrib
projects[memcache][version] = 1.0

projects[metatag][subdir] = contrib
projects[metatag][version] = 1.0-beta9

projects[module_filter][subdir] = contrib
projects[module_filter][version] = 2.0-alpha2

projects[mollom][subdir] = contrib
projects[mollom][version] = 2.10

projects[navbar][type] = "module"
projects[navbar][subdir] = "contrib"
projects[navbar][version] = "1.4"

; Change I-beam cursors in the navbar to be a pointer instead.
; https://drupal.org/node/2173041
projects[navbar][patch][] = "https://drupal.org/files/issues/2173041-3-i-beam-menu-hover.patch"

projects[nodequeue][subdir] = contrib
projects[nodequeue][version] = 2.x-dev

projects[oembed][type] = "module"
projects[oembed][subdir] = "contrib"
projects[oembed][download][type] = "git"
projects[oembed][download][url] = "http://git.drupal.org/project/oembed.git"
projects[oembed][download][branch] = "7.x-1.x"
projects[oembed][download][revision] = "9aa5303"

; Remove the media submodule as it conflicts with the Media: oEmbed module.
; https://drupal.org/node/2269745#comment-8796261
projects[oembed][patch][] = "https://drupal.org/files/issues/remove-media-submodule-2269745-2.patch"

projects[paranoia][type] = "module"
projects[paranoia][subdir] = "contrib"
projects[paranoia][version] = "1.3"

projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.2

projects[picture][type] = "module"
projects[picture][subdir] = "contrib"
projects[picture][download][type] = "git"
projects[picture][download][url] = "http://git.drupal.org/project/picture.git"
projects[picture][download][branch] = "7.x-1.x"
projects[picture][download][revision] = "18b94b9"

projects[picture][version] = "1.x-dev"
projects[picture][type] = "module"
projects[picture][subdir] = "contrib"
projects[picture][download][type] = "git"
projects[picture][download][revision] = "3d9fe6c"
projects[picture][download][branch] = "7.x-1.x"

projects[plupload][version] = "1.3"
projects[plupload][type] = "module"
projects[plupload][subdir] = "contrib"

projects[privatemsg][subdir] = contrib
projects[privatemsg][version] = 2.x-dev
; Add Full public CRUD API for private message module.
; https://drupal.org/node/1184984
projects[privatemsg][patch][] = "https://drupal.org/files/1184984-privatemsg_crud-11.patch"

projects[queue_mail][subdir] = contrib
projects[queue_mail][version] = 1.2

projects[radioactivity][subdir] = contrib
projects[radioactivity][version] = 2.9

projects[redirect][subdir] = contrib
projects[redirect][version] = 1.0-rc1

projects[remote_stream_wrapper][subdir] = contrib
projects[remote_stream_wrapper][version] = 1.0-rc1

projects[responsive_preview][type] = module
projects[responsive_preview][subdir] = contrib
projects[responsive_preview][version] = 1.1

projects[rules][subdir] = contrib
projects[rules][version] = 2.7

projects[schemaorg][type] = "module"
projects[schemaorg][subdir] = "contrib"
projects[schemaorg][version] = "1.0-beta4"

projects[search_facetapi][type] = "module"
projects[search_facetapi][subdir] = "contrib"
projects[search_facetapi][version] = "1.0-beta2"

projects[smartcrop][type] = "module"
projects[smartcrop][subdir] = "contrib"
projects[smartcrop][version] = "1.0-beta2"

projects[smiley][subdir] = contrib
projects[smiley][version] = 1.0

projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"
projects[strongarm][download][type] = "git"
projects[strongarm][download][url] = "http://git.drupal.org/project/strongarm.git"
projects[strongarm][download][branch] = "7.x-2.x"
projects[strongarm][download][revision] = "5a2326ba67e59923ecce63d9bb5e0ed6548abdf8"

projects[timeago][type] = "module"
projects[timeago][subdir] = "contrib"
projects[timeago][version] = "2.2"

; Provide a dedicated date type.
; http://drupal.org/node/1427226#comment-6638836
projects[timeago][patch][] = "http://drupal.org/files/1427226-timeago-date-type.patch"

projects[title][subdir] = contrib
projects[title][version] = 1.0-alpha7

projects[token][subdir] = contrib
projects[token][version] = 1.5

projects[transliteration][subdir] = contrib
projects[transliteration][version] = 3.2

projects[userpoints][subdir] = contrib
projects[userpoints][version] = 1.0

projects[variable][subdir] = contrib
projects[variable][version] = 2.5

projects[views][subdir] = contrib
projects[views][version] = 3.8

; Update Views Content access filter per core performance improvements.
; https://drupal.org/comment/8516039#comment-8516039
projects[views][patch][] = "https://drupal.org/files/issues/views-content_access_filter_per_core_performance-2204257-4_0.patch"

projects[views_bulk_operations][subdir] = contrib
projects[views_bulk_operations][version] = 3.2

projects[views_litepager][subdir] = contrib
projects[views_litepager][version] = 3.0

projects[views_load_more][subdir] = contrib
projects[views_load_more][version] = 1.2

projects[views_rss][subdir] = contrib
projects[views_rss][version] = 2.0-rc3

projects[views_slideshow][subdir] = contrib
projects[views_slideshow][version] = 3.1

projects[webform][subdir] = contrib
projects[webform][version] = 3.18

projects[wordfilter][subdir] = contrib
projects[wordfilter][version] = 1.x-dev

projects[xbbcode][subdir] = contrib
projects[xbbcode][version] = 1.x-dev


; Development
; TODO: move to a -dev make file
projects[devel][subdir] = development
projects[devel][version] = 1.x-dev

projects[XHProf][type] = module
projects[XHProf][subdir] = development
projects[XHProf][version] = 1.x-dev


; Theme
projects[zen][type] = "theme"
projects[zen][subdir] = "contrib"
projects[zen][version] = "5.5"

projects[shiny][type] = "theme"
projects[shiny][subdir] = "contrib"
projects[shiny][version] = "1.4"


; Libraries
; NOTE: These need to be listed in http://drupal.org/packaging-whitelist.
libraries[backbone][download][type] = "get"
libraries[backbone][type] = "libraries"
libraries[backbone][download][url] = "https://github.com/jashkenas/backbone/archive/1.1.2.tar.gz"

libraries[ckeditor][download][type] = "get"
libraries[ckeditor][type] = "libraries"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.3.4/ckeditor_4.3.4_full.zip"

libraries[ckeditor_bbcode][download][type] = "get"
libraries[ckeditor_bbcode][type] = "libraries"
libraries[ckeditor_bbcode][download][url] = "http://download.ckeditor.com/bbcode/releases/bbcode_4.3.3.zip"

libraries[modernizr][download][type] = "get"
libraries[modernizr][type] = "libraries"
libraries[modernizr][download][url] = "https://github.com/Modernizr/Modernizr/archive/v2.7.2.tar.gz"

libraries[timeago][download][type] = "get"
libraries[timeago][type] = "libraries"
libraries[timeago][download][url] = "https://raw.github.com/rmm5t/jquery-timeago/v1.4.1/jquery.timeago.js"

libraries[underscore][download][type] = "get"
libraries[underscore][type] = "libraries"
libraries[underscore][download][url] = "https://github.com/jashkenas/underscore/archive/1.6.0.zip"

libraries[jquery_cycle][type] = libraries
libraries[jquery_cycle][download][type] = "get"
libraries[jquery_cycle][download][url] = "http://malsup.github.com/jquery.cycle.all.js"
libraries[jquery_cycle][filename] = "jquery.cycle.all.js"
libraries[jquery_cycle][directory_name] = "jquery.cycle"